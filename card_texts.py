card1 = '''There goes the albatross, soaring through the skies with its wings outstretched, capturing the essence of freedom and the thrill of travel. This majestic bird, unhindered by boundaries, embodies the call of the wild — an invitation to journey into the vast, open world. Just like the albatross, you, too, are ready for your next grand adventure. 

So let this image be the sign you've been waiting for: it's time to pack your bags, grab your passport (and maybe a snack or two), and get ready to spread your wings — who knows what hilariously epic tales and unforgettable memories await beyond the horizon? It’s your time to soar, explore like Columbus (without getting lost), and unleash your inner albatross!
'''

card2 = '''This card shows how analytics can make predictions about the future. But what kind of future will it predict? Will it be about money or your life and happiness? You decide! Drawing this card means you're good at noticing things, giving you a boost of +20 points in intuition. Pay attention to your desires because they might just come true!'''

card3 = '''The tower on this card signifies the power of uniting in diversity (just like all of us here at ETG). It's proof that even if you're as different as pickles and ice cream, you can still make magic happen when you've got a shared goal. This card encourages you to think so far out of the box that you can’t even see the box and pushes you to keep collaborating with your peers around the world. Remember: with trust, love, and a good dose of coffee, you can accomplish great things alongside those who share your drive.'''

card4 = '''The card is a riot! It's like discovering a treasure map with a pile of cash or winning the lottery. Wave goodbye to doubts. You’re hitting KPIs and TCOs like peanuts, graphs shoot skyward just at your glance, and partners are on board with all your proposals. Chuck Norris is standing by, in awe of your confidence. You're ready for a ride filled with cash and success.'''

card5 = '''Chuck Norris flashing his confident smile while riding a horse — what a winning combo! It's a jackpot of coolness and swagger all in one. Let that Chuck Norris energy awaken inside you, boosting your confidence to the moon. You don't need to impress anyone or play by anyone else's rules — those are just background noise. Trust your instincts, think outside the box, and charge forward fearlessly like Chuck Norris on horseback.'''

card6 = '''This card depicts a lively court scene. Every day, we encounter situations where we have to make decisions. You may have something weighing on your mind at the minute. Whether you're deliberating an epic life decision or simply pondering between pizza and tacos for dinner, remember — you've got this! Consider yourself the witty judge of your own choices. Let this card be your quirky guide through the daily courtroom of choices life presents as you are the CEO and the judge of your life adventure.'''

card7 = '''You have drawn a card with a marketing genius who sparks your creative passion. From now on, you'll be energized with creativity and original ideas. You can find a unique solution in a work project or in your life. From ideas of things to do in new places to jokes that cheer up your friends or maybe even new ways to make money. It's up to you!'''

card8 = '''Seagull alert! If you hear their wails, it's time to sail away from the chaos and give yourself some TLC — tender loving care. This card is your sign to ditch the squawking seagulls of stress and make room for serenity: indulge in a spa day, venture beyond the noisy shorelines, or simply hang out with your favorite fellows. Protect your peace and keep those sobbing seagulls at bay — rest in a seagull-free zone!'''

card9 = '''What a pawsome vibe in this furry frenzy card — like a pet lover's dream come true! These critter buddies want to remind you to find joy in the little things and sometimes be as carefree as a playful pup. There's laughter and kindness in the everyday hustle and bustle amidst the chaos of routine tasks and work. Why don't you start searching right now? 

By the way, join this #fun-pets channel for an extra shot of pawsomeness and cuteness overload. Woof-woof!
'''

card10 = '''This card illustrates a scene of ascending graphs presented to an engaged audience, symbolizing growth and positive changes in your life. It's your moment to channel your inner Chuck Norris – brace yourself to dive fearlessly into uncharted territories, smash through barriers, and emerge as the fearless CEO of your success story. Your path to greatness is wide open and ready for your epic takeover. Are you ready to rock the world?'''

card11 = '''Balance is key — just like tending to a garden. You've got to plan when to plant those seeds of hard work and when it's time to kick back and rest a bit. Some days, you need to pull out the weeds. And, on other days, lying back, feeling the sun on your face, and watching the flowers is okay, too…

Remember to enjoy the moments, big and small. Good luck finding your groove, and may you always have time for a coffee break and a smile. Keep it simple, keep it balanced, and everything else will fall into place, one day at a time.
'''

card12 = '''Hey, you're a lucky one — this card is the shining star of the deck that sprinkles love and mojo for getting things done! It is like the secret sauce delivering an out-of-this-world dose of magical energy. With this card, you're armed with the power of ETG to conquer challenges and turn ordinary moments into extraordinary adventures. Just embrace the love surrounding you, and may the hidden possibilities be open. When you've got the ETG crew cheering you on, there are no limits to what you can achieve.'''

card13 = '''Get those space helmets on and buckle up because we're about to launch a rocket ship to the wild, wacky corners of the universe — better pack some snacks!

This inspiring card captures the essence of propelling beyond the familiar, showing that a momentous change or brilliant idea is just within grasp. Get ready for a rush of freedom and a shift that promises to infuse your life with wonder and positivity. So, strap in tight and hold on to your socks — cause we're about to blast off into a chapter of adventures that'll have you shouting “wheeeee” all the way to the stars!
'''

card14 = '''Behold the seagull, entrepreneur of the skies, perched confidently on a chest of gold, guarding it like the Finance team guards our budget. This symbolizes the prosperity that awaits when you chase your passions. Armed with determination and dreams, you're set to win big.

So, let your ambitions set sail as you navigate the open seas of possibility. Just remember to keep an eye out for those metaphorical treasure chests. And if you happen to spot a seagull looking suspiciously wealthier than usual, perhaps it's time to ask for investment tips — or at least a few pointers on perfecting that confident, wind-ruffled hair look.
'''

card15 = '''The vase in this card tried to break the record for the most cracks per square inch. But guess what? Those cracks are just openings for some out-of-the-box thinking — they're like secret passageways leading to innovative solutions! Trust the process, enjoy the crazy rollercoaster ride, and keep on trucking through life's ups and downs.

If you feel like you’ve been stuck in a 10-kilometer traffic jam with no chance of escaping soon, let this be a sign that an effortless and remarkable choice is emerging. And hey, whether you go left, right, or take a detour through Narnia, just remember—the journey is half the fun! 
'''

card16 = '''The card shows a person basking in the spotlight with an adoring audience. It's a gentle reminder that you're the main character in your own life. Prepare for applause, Thanks on the Intranet, and cheers — the ETG universe has something for you. So, it's time to become a true rockstar of your everyday adventures. After all, you always matter whether you're heating a new FNBC target or just enjoying your morning coffee. The ETG team stands ready to support you — just shine bright!'''

card17 = '''You have drawn a card with a magician carrying a bag full of EBITDA. This is special EBITDA collected from the best deals from the most profitable partners. How lucky! From now on, you can expect nothing but wealth and prosperity. Be wise in your spending, try to increase your wealth, and invest more in yourself.'''

card18 = '''The card depicts a team warmly greeting a new office member — a hint that you might soon have a fresh face on your team or meet an ETG companion. Either way, teamwork is the secret ingredient in conquering tough tasks (but meeting for coffee and small talk also counts). Gear up, join the fun, and prepare for adventures beyond frontiers with your ETG crew. Remember, together, there's no obstacle you can't overcome!'''

card19 = '''Wow, it looks like someone’s got a cactus ice cream cone — a beloved metaphor for no sugar-coating at ETG. 

Life takes many twisted turns and bumpy roads, making you wish for more clarity to fill up your life and bring convenience and ease with it. If you are wading through a sea of confusion, take this card as a sign that crystal-clear understanding is just around the corner. And it will come just as we prefer it served — only facts, said like they are!
'''

card20 = '''The card features a cheerful Atlant balancing the entire planet on his shoulders, indicating you're an indispensable part of your crew. Life sometimes makes you feel like you're juggling the weight of the world. But hey, that's just one side of the coin! The flip side reveals that your interest in your work will lead to ingenious solutions and recognition for your efforts. So, keep rockin' and rollin' through the ETG universe with a stellar attitude! The world might be heavy, but your spirit is weightless!'''


card21 = '''In the grand party of life, meeting new people is like discovering a new favorite song. 
It might start with just a few notes of small talk, and then suddenly, you're dancing to the rhythm of a new friendship. Finding new partners — whether for projects, games, or just for that morning coffee run — is like adding harmony to the melody; together, you create a richer and more fun tune. Cheers to new friends, shared laughs, and the stories to come!
'''

card22 = '''“In the bustling marketplace, amidst the swirl of merchants and townsfolk, there walked a man with parchment in hand.” This card represents an agent conducting market research. He records everything he sees and analyzes it. By pulling this card, you get a bonus to emotional intelligence and situational awareness. No question or problem will ever catch you off guard again. After all, you are now prepared like no other!'''

card23 = '''Meeting a robot for the first time is like stumbling into a scene from a zany sci-fi flick — the future is now, and those robot buddies are knocking at your door! This card is a quirky nudge to welcome these mechanical marvels into your world. Your task-handling game is about to get a turbo-charged upgrade. Step into futuristic fun, and let the AI spark a newfound passion for work and play! It's time to shake up your routine and dive headfirst into the whimsical wonders of the digital age.'''

card24 = '''This card depicts a salesman in the middle of a negotiation. Drawing this card gives you a bonus for your negotiation skills. Make sure to use them wisely, as the importance of this soft skill cannot be overstated. You might close a million-dollar deal or come to an agreement with seagulls for unlimited fish deliveries.'''

card25 = '''The card captures a happy runner who's just hit the ground running. It's a sign that you're geared up for success and ready to conquer whatever hurdles come your way, whether you've just begun a sprint in development, started work on a new project, or joined the race to finish that giant-sized pizza. Trust in your hustle and know you can tackle anything at full speed. So, lace up those sneakers and smile through the struggle — it's time to sprint towards greatness, leaving any doubts in the dust behind!'''

card26 = '''This card reveals a Supply Manager gazing at a massive stack of hotels we've acquired. What a find! Drawing this card grants you a prosperity bonus. But beware: if you've never dreamt of becoming a tycoon landlord, brace yourself because this stack is on its way. When you return home, ensure your living space is cozy enough because, from now on, all the top hotels will try to match your apartment's style.'''

card27 = '''In this warm, inviting scene, we see the embodiment of guidance and support, a smooth beginning on a collaborative journey. 

We're here to help each other out, and when we team up, we can do amazing things. Like Chuck Norris, we're strong on our own but unstoppable when working together.
Let's tackle every challenge with a smile and a laugh and turn each day into a win — together. Here's to doing great things and enjoying every step of the journey.
'''

card28 = '''What a view we have here! A neat workplace that sparkles brighter than a freshly polished disco ball — you might even need sunglasses to shield your eyes from the blinding aura of perfection. It seems like everything is set up just perfectly to get caught up in a whirlpool of projects that'll have you saying, “Dusk 'til dawn? More like dawn 'til dusk!”

But hey, how about slowing down just a little bit? It's your VIP ticket to the self-care city, a gentle nudge to hit the pause button and say, “Heck yeah, I'm awesome!”. Even Chuck Norris has to rest, as productivity flourishes not just in doing but also in resting.
'''

card29 = '''This image serves as a reminder to wait, sit back, and have another think — good decisions need that moment of calm. There’s no need to rush into your next move. Life isn’t about who’s the quickest; it's about making choices that fit your life best. So, take a breather, let those brain gears whir for a bit longer, and who knows, you might uncover a genius idea or finally figure out how to win in a Monopoly game…

It's like giving your masterpiece one final check for typos — because, let's be real, “sent with love” is far more endearing than “sent with a facepalm-inducing typo that will go down in chat history!”
'''

card30 = '''Knock-knock! This is a kind reminder to take care of your health and well-being! Who said staying healthy and sane while working from home can't be fun? 
Desk-ercise: Your chair can double as a gym buddy. Take “leg lift” breaks between emails.
Walkie-talkie Meetings: Pace around during phone calls. It’s walking with a purpose, making you sound more important.
Vitamin D(og): Pet your dog or cat. No pet? No problem. A stuffed animal works, and it won't judge your spreadsheet skills.
Remember, your home is your castle — and that office chair is your throne. Rule it with a healthy dose of fun and wellness and may your coffee be strong and your Wi-Fi stronger! Keep on thriving in your home office adventures!
'''

card31 = '''Do you have some concerns in your mind right now? Is something bothering you? Well, then, it's your sign that everything's gonna work out in the best way possible. It's like one of your fellow ETG colleagues giving you a high-five and saying, “Let's roll!”.

Trust the universe's quirky sense of humor — whether you're navigating a project deadline, planning a trip, or deciding what's for dinner. Life's cosmic chuckle is guiding you. So, dude, relax and enjoy the ride. 
'''

card32 = '''Remember when we used to meet now and then to spend time together? It seems like a lifetime in the era of virtual hangouts and pajama-pants Zoom calls! 

This card is a harbinger of the shift from pixels to the tangible, signaling that soon you'll swap clicks for clinks, and emoji chats will morph into coffee dates with actual mugs. Anticipate the fun of offline meet-ups where new faces become new peers. It's a promise that real-life laughs and the warmth of companionship are just around the corner.
'''

card33 = '''Amid the whirlwind of remote work, we all know the feeling of being swamped. This card is a reminder to embrace the chaos and add some funny stuff into your daily routine: 

Give yourself points for speed, style, and not burning lunch, and get a special reward at the end of the day!
Embrace the mess in your living room — it's all part of the creative chaos! 
Use headphones to create an imaginary bubble. During calls, mute is the magic button to silence kids, pets, and alien invasions.
Got kids? They're now your tiny, adorable assistants. Teach 'em young! Got a dog? Train it to sort emails.

Remember, the key is to mix structure with a splash of chaos and a good dose of humor. The juggle is real, but so is the joy of occasionally working in your superhero slippers!
'''

