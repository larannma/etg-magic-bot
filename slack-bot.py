import slack
from flask import Flask, request, jsonify
import random
import threading
import json
import os
from dotenv import load_dotenv
from slackeventsapi import SlackEventAdapter
from pymongo import MongoClient
from bson.objectid import ObjectId
import requests
from slack import WebClient
import gspread
from google.oauth2.service_account import Credentials

import card_texts
import images_subtitle

scopes = ["https://www.googleapis.com/auth/spreadsheets"]
creds = Credentials.from_service_account_file("credentials.json", scopes=scopes)
client = gspread.authorize(creds)
sheet_id = "16fIsWPG_q_rFiRZ9cuWYXYFVn13ZooG0IczbFp5NDHY"
sheet = client.open_by_key (sheet_id)
values_list = sheet.sheet1.row_values(1)

app = Flask(__name__)
load_dotenv()
slack_token = os.getenv("SLACK_BOT_TOKEN")


slack_client = WebClient(token=os.getenv("SLACK_BOT_TOKEN"))
slack_event_adapter = SlackEventAdapter(os.getenv("SLACK_SIGNING_SECRET"), '/slack/events', app)

image_urls = [
    [images_subtitle.urls[0], card_texts.card1],
    [images_subtitle.urls[1], card_texts.card2],
    [images_subtitle.urls[2], card_texts.card3],
    [images_subtitle.urls[3], card_texts.card4],
    [images_subtitle.urls[4], card_texts.card5],
    [images_subtitle.urls[5], card_texts.card6],
    [images_subtitle.urls[6], card_texts.card7],
    [images_subtitle.urls[7], card_texts.card8],
    [images_subtitle.urls[8], card_texts.card9],
    [images_subtitle.urls[9], card_texts.card10],
    [images_subtitle.urls[10], card_texts.card11],
    [images_subtitle.urls[11], card_texts.card12],
    [images_subtitle.urls[12], card_texts.card13],
    [images_subtitle.urls[13], card_texts.card14],
    [images_subtitle.urls[14], card_texts.card15],
    [images_subtitle.urls[15], card_texts.card16],
    [images_subtitle.urls[16], card_texts.card17],
    [images_subtitle.urls[17], card_texts.card18],
    [images_subtitle.urls[18], card_texts.card19],
    [images_subtitle.urls[19], card_texts.card20],
    [images_subtitle.urls[20], card_texts.card21],
    [images_subtitle.urls[21], card_texts.card22],
    [images_subtitle.urls[22], card_texts.card23],
    [images_subtitle.urls[23], card_texts.card24],
    [images_subtitle.urls[24], card_texts.card25],
    [images_subtitle.urls[25], card_texts.card26],
    [images_subtitle.urls[26], card_texts.card27],
    [images_subtitle.urls[27], card_texts.card28],
    [images_subtitle.urls[28], card_texts.card29],
    [images_subtitle.urls[29], card_texts.card30],
    [images_subtitle.urls[30], card_texts.card31],
    [images_subtitle.urls[31], card_texts.card32],
    [images_subtitle.urls[32], card_texts.card33]
]

# Настройка соединения с MongoDB локально
mongo_client = MongoClient('mongodb://localhost:27017/')
db = mongo_client['slack_app']
tokens_collection = db['user_tokens']
users_collection = db['users']

# Настройка с mongo с Docker

# mongo_client = MongoClient(
#     'mongodb://mongoadmin:secret@some-mongo:27017/'
# )
# db = mongo_client['slack_app']
# tokens_collection = db['user_tokens']
# users_collection = db['users']


def get_access_token(team_id):
    token_info = tokens_collection.find_one({'team_id': team_id})
    if token_info:
        return token_info['access_token']
    else:
        return None
    
@app.route('/', methods=['GET'])
def check_route():
    return "Главная страница "

@app.route('/slack/oauth_redirect', methods=['GET'])
def slack_oauth_redirect():
    code = request.args.get('code')
    response = requests.post("https://slack.com/api/oauth.v2.access", data={
        "client_id": os.getenv('SLACK_CLIENT_ID'),
        "client_secret": os.getenv('SLACK_CLIENT_SECRET'),
        "code": code
    }).json()
    
    if not response.get("ok"):
        return "Ошибка авторизации: " + response.get("error", "Неизвестная ошибка"), 500
    
    team_id = response['team']['id']
    access_token = response['access_token']  # Получаем access_token

    # Сохраняем токен в MongoDB
    tokens_collection.update_one(
        {'team_id': team_id},
        {'$set': {'access_token': access_token}},
        upsert=True
    )
    
    return 'OAuth Flow Completed'

def send_message(channel_id, user_id, message):
    access_token = get_access_token(user_id)
    if access_token:
        local_client = slack.WebClient(token=access_token)
        local_client.chat_postMessage(channel=channel_id, text=message)

def send_image_and_button(channel_id, user_id, access_token):
    selected_field = random.choice(image_urls)
    selected_image_url = selected_field[0]
    if access_token:
        local_client = slack.WebClient(token=access_token)
        try:
            response = local_client.users_info(user=user_id)
            user_profile = response['user']['profile']
            user_email = user_profile.get('email')
            try:
                sheet.sheet1.append_row([user_email])
            except:
                print('could not to add value to google sheets')
            if not user_email:
                print("Email not available for user.")
                return
        except:
            print('some error')
        try:
            response = local_client.chat_postMessage(
                channel=channel_id,
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"<@{user_id}> Here’s your card🪄"
                        }
                    },
                    {
                        "type": "image",
                        "image_url": selected_image_url,
                        "alt_text": "inspirational card"
                    }
                    ,
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ("Before delving into the card's meaning, take a moment to reflect:"
                                        "\n"
                                        "🔮 What do you observe within this card?" 
                                        "\n"
                                        "🔮 How does it make you feel?"
                                    )
                        }
                    },
                    {
                        "type": "actions",
                        "elements": [
                            {
                                "type": "button",
                                "text": {
                                    "type": "plain_text",
                                    "text": "Unveil the Magic",
                                },
                                "action_id": "show_wish_button",
                                "value": selected_field[1]
                            }
                        ]
                    }
                ]
            )
            return response['ts'], selected_image_url
        except slack.errors.SlackApiError as e:
            print(f"Error sending image and button: {e.response['error']}")
    else:
        print("No access token provided.")
        return None, None

def display_wish(channel_id, team_id, user_id, message_ts, image_url, access_token, selected_text):
    if access_token:
        local_client = slack.WebClient(token=access_token)
        try:
            local_client.chat_update(
                channel=channel_id,
                ts=message_ts,
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"<@{user_id}> 🪄"
                        }
                    },
                    {
                        "type": "image",
                        "image_url": image_url,
                        "alt_text": "Random Image"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"<@{user_id}> *the Magic:* \n{selected_text}"
                        }
                    }
                ]
            )
        except Exception as e:
            print(f"Error updating message: {e}")
    else:
        print("No access token provided for the team.")

@app.route('/new_card', methods=['POST'])
def new_card():
    data = request.form
    user_id = data.get('user_id')
    channel_id = data.get('channel_id')
    team_id = data.get('team_id')
    access_token =  get_access_token(team_id)

    if access_token:
        threading.Thread(target=send_image_and_button, args=(channel_id, user_id, access_token)).start()
        return '', 204
    else:
        print(f"Token for team_id {team_id} not found.")
        return 'Token not found', 404

@app.route('/interactive-endpoint', methods=['POST'])
def interactive_endpoint():
    payload = json.loads(request.form['payload'])
    action = payload['actions'][0]
    if action['action_id'] == 'show_wish_button':
        channel_id = payload['channel']['id']
        team_id = payload['team']['id']
        user_id = payload['user']['id']
        message_ts = payload['message']['ts']
        image_url = payload['message']['blocks'][1]['image_url']
        selected_text = action['value']

        access_token = get_access_token(team_id)

        threading.Thread(target=display_wish, args=(channel_id, team_id, user_id, message_ts, image_url, access_token, selected_text)).start()

    return '', 204

@slack_event_adapter.on('app_home_opened')
def handle_app_home_opened(event_data):
    user_id = event_data['event']['user']
    team_id = event_data['team_id']

    access_token = get_access_token(team_id)
    if not access_token:
        print(f"Access token for team {team_id} not found.")
        return

    local_client = WebClient(token=access_token)

    if not users_collection.find_one({'user_id': user_id}):
        
        try:
            local_client.chat_postMessage(
                channel=user_id,
                text="Welcome to the best app on Slack!",
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": (
                                "Hey there!\n"
                                "Ready to sprinkle your day with a dose of magic? I'm here to help! 🌚\n"
                                "\n"
                                "Simply summon me with `/new-card`, and brace yourself for a metaphoric card that might\n"
                                "just light up your path, answer a burning question, or simply bring a smile to your face.\n"
                                "\n"
                                "Don't take it too seriously, though – just sit back, relax, and let the fun begin! 🔮\n"
                            )
                        }
                    }
                ]
            )
            users_collection.insert_one({'user_id': user_id})
        except Exception as e:
            print(f"Error sending direct message: {str(e)}")


@app.route('/slack/events', methods=['POST'])
def slack_events():
    return slack_event_adapter.server

if __name__ == '__main__':
    app.run(debug=True)
