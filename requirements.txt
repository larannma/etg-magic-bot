Flask==2.3.3
requests==2.31.0
slackclient==2.9.4
slackeventsapi==3.0.1
pymongo==4.6.2
python-dotenv==1.0.1
gunicorn==20.1.0
gspread==6.0.2
